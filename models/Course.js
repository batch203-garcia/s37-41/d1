const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is required."]
    },
    description: {
        type: String,
        required: [true, "Description is required."]
    },
    price: {
        type: String,
        required: [true, "Price is required."]
    },
    slots: {
        type: String,
        required: [true, "Slots is required."]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdON: {
        type: Date,
        default: new Date()
    },
    enrollees: [{
        userId: {
            type: String,
            required: [true, "User ID  is required."]
        },
        isPaid: {
            type: Boolean,
            default: true
        },
        dateEnrolled: {
            type: Date,
            default: new Date()
        },
    }]
});

module.exports = mongoose.model("Course", courseSchema);