const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

//email check
console.log(userControllers);
router.post("/checkEmail", userControllers.checkEmailExists);

//user registration
router.post("/register", userControllers.registerUser);

//user Auth
router.post("/login", userControllers.loginUser);

//view user by ID
router.get("/details", auth.verify, userControllers.getProfile);

//user -> enroll to a course
router.post("/enroll", auth.verify, userControllers.enroll);

module.exports = router;