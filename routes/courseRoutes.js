const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

console.log(courseControllers);
router.post("/", courseControllers.addCourse);
router.get("/all", auth.verify, courseControllers.getAllCourses);
router.get("/", courseControllers.getAllActive);
router.get("/:courseId", courseControllers.getCourse);
router.put("/:courseId", auth.verify, courseControllers.updateCourse);
router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);


module.exports = router;