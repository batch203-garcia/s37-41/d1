const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.createAccessToken = (user) => {
    console.log(user)
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {});

}



//middleware function
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    //if (token !== undefined) 
    if (typeof token !== "undefined") {
        token = token.slice(7, token.length);
        console.log(token);

        return jwt.verify(token, secret, (err, data) => {
            if (err) {
                return res.send({ auth: "Invalid token!" });
            } else {
                next();
            }
        });



    } else {
        res.send({ message: "Authentication failed! No token provided!" });
    }

    console.log(token);
}

module.exports.decode = (token) => {
    if (token !== undefined) {
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if (err) {
                return null;
            } else {
                return jwt.decode(token, { complete: true }).payload;
            }
        });
    }
}