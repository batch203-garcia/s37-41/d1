const Course = require("../models/Course.js");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/


module.exports.addCourse = (req, res) => {
    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots,
    });

    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        newCourse.save()
            .then(course => {
                console.log(course);
                res.send(true);
            })
            .catch(err => {
                console.log(err);
                res.send(false);
            });

    } else {
        res.send("You don't have the acces to this page!");
    }

}

module.exports.getAllCourses = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        return Course.find({}).then(result => res.send(result));
    } else {
        res.send("You don't have the acces to this page!");
    }
}

//retrieve all active courses
module.exports.getAllActive = (req, res) => {
    return Course.find({ isActive: true }).then(result => res.send(result));

}

//retrieve a specific course
module.exports.getCourse = (req, res) => {
    console.log(req.params.courseId);
    return Course.findById(req.params.courseId).then(result => res.send(result));

}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body

*/

//update a course
module.exports.updateCourse = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        let updateCourse = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            slots: req.body.slots,
        }
        return Course.findByIdAndUpdate(req.params.courseId, updateCourse, { new: true })
            .then(result => {
                console.log(result);
                res.send(result);
            })
            .catch(error => {
                console.log(error);
                res.send(false);
            });
    } else {
        res.send("You don't have the acces to this page!");
    }
}

//archiving a course -> soft delete
module.exports.archiveCourse = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let updateIsActiveField = {
        isActive: req.body.isActive
    }

    if (userData.isAdmin) {
        return Course.findByIdAndUpdate(req.params.courseId, updateIsActiveField)
            .then(result => {
                console.log(result);
                res.send(true);
            })
            .catch(error => {
                console.log(error);
                res.send(false);
            });
    } else {
        res.send("You don't have the acces to this page!");
    }
}