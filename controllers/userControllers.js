const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/Course.js");
const { findOne } = require("../models/User.js");

//Check if email is taken

module.exports.checkEmailExists = (req, res) => {
    return User.find({ email: req.body.email })
        .then(result => {
            console.log(result);

            if (result.length > 0) {
                //return res.send(true);
                return res.send("User already exists.");
            } else {
                //return res.send(false);
                return res.send("No duplicate found.");
            }
        })
        .catch(error => res.send(error));
}

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {


    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNumber: req.body.mobileNumber
    });

    console.log(newUser);
    return newUser.save()
        .then(user => {
            console.log(user);
            res.send(true);
        })
        .catch(error => {
            console.log(error);
            res.send(false);
        })
}

module.exports.loginUser = (req, res) => {
    return User.findOne({ email: req.body.email })
        .then(result => {
            if (result == null) {
                return res.send({ message: "No user found!" });
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(result) });
                } else {
                    return res.send({ message: "Incorrect password!" });
                }
            }
        })
}

//S38 ACTIVITY

//View User by ID

/* module.exports.getProfile = (req, res) => {
    console.log(req.params.id);

    return User.findById(req.params.id).then(result =>{
        result.password = "***";
        res.send(result);
    })
} */


module.exports.getProfile = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    return User.findById(userData.id).then(result => {
        result.password = "***";
        res.send(result);
    })
}

//s41
// Enroll a Course
/*
	Steps:
	// Users
	1. Find the document in the database using the user's ID (token)
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
	//Courses
	1. Find the document in the database using the course ID provided in the request body.
	2. Add the user ID to the course enrollees array.
	3. Update the document in the MongoDB Atlas Database
*/

module.exports.enroll = async(req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let courseName = await Course.findById(req.body.courseId)
        .then(result => result.name);


    let data = {
        userId: userData.id,
        email: userData.email,
        courseId: req.body.courseId,
        courseName: courseName
    }
    console.log(data);
    let isUserUpdated = await User.findById(data.userId)
        .then(user => {
            user.enrollments.push({
                courseId: data.courseId,
                courseName: data.courseName,
            });
            return user.save()
                .then(result => {
                    console.log(result);
                    return true;
                })
                .catch(error => {
                    console.log(error);
                    return false;
                })
        });

    console.log(isUserUpdated);
    let isCourseUpdated = await Course.findById(data.courseId)
        .then(course => {
            course.enrollees.push({
                    userId: data.userId,
                    email: data.email
                })
                //minus slot by 1
            course.slots -= 1;
            return course.save()
                .then(result => {
                    console.log(result);
                    return true;
                })
                .catch(error => {
                    console.log(error);
                    return false;
                })
        });
    console.log(isCourseUpdated);

    //(isUserUpdated && isCourseUpdated) ? res.send(true) : res.send(false) -> alternative to if/else statement -> ternary

    if (isCourseUpdated && isUserUpdated) {
        res.send(true);
    } else {
        res.send(false);
    }

}